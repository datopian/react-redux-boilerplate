import { default as appState } from "./appState"
import { combineReducers } from "redux"

export default combineReducers({ appState })
