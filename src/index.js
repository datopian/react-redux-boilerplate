import React from "react"
import ReactDOM from "react-dom"
import "./App.css"
import AppWithProvider from "./AppWithProvider"
import * as serviceWorker from "./serviceWorker"

ReactDOM.render(<AppWithProvider />, document.getElementById("react-app"))

serviceWorker.unregister()
